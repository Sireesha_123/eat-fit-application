package com.eat.entity;

public enum LoginStatusEnum {
	LOGGED_IN,
	LOGGED_OUT;
}
