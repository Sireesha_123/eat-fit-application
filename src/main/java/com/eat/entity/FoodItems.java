package com.eat.entity;



import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodItems {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int FoodID;
	private String FoodType;
	private String FoodName;
	private int Price;
	
	
	@JoinColumn(name="venderID", referencedColumnName="venderID")
	//@JoinColumn(name="Vender_emailId", referencedColumnName="emailId")
	@ManyToOne
	private Vendor vender;


}
