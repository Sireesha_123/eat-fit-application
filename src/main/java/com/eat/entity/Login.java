package com.eat.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 
@Getter
@Setter
@Data
@NoArgsConstructor
@Entity
public class Login {
	
	
	public Login(String emailId, String password, RolesEnum role, LoginStatusEnum loginStatus) {
		super();
		this.id = id;
		this.emailId = emailId;
		this.password = password;
		this.role = role;
		this.loginStatus = loginStatus;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String emailId;
	private String password;
	@Enumerated(EnumType.STRING)
	private RolesEnum role;
	@Enumerated(EnumType.STRING)
	private LoginStatusEnum loginStatus;
	
}
