package com.eat.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class Customer {
       @Id
   	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerId;
	private String CustomerName;
	private long contactNo;
	private String emailId;
	private String password;
	private String Role;
//	@Enumerated(EnumType.STRING)
//	private LoginStatusEnum loginStatus;
}