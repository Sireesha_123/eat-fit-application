package com.eat.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Vendor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int venderID;
	private String VenderName;
	private Long PhoneNo;
	private String emailId;
	private String Password;
	private String Role;
	
//	@Enumerated(EnumType.STRING)
//	private LoginStatusEnum loginStatus;	
}
