package com.eat.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eat.entity.Login;
import com.eat.entity.Vendor;



public interface VenderRepository extends JpaRepository<Vendor,Integer>{

	Optional<Vendor> findByemailId(String emailId);

	Optional<Login> findByvenderID(Integer venderID);

	//Optional<Login> findByVenderID(Integer venderID);


		
	}


