package com.eat.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eat.dto.LoginDTO;
import com.eat.entity.Login;
import com.eat.entity.LoginStatusEnum;
;

public interface LoginRepository extends JpaRepository<Login, Long>{

	Optional<Login> findByEmailIdAndPassword(String email, String password);
	 
	void save(LoginDTO loginDTO);

	

	//Optional<Login> findByEmailIdAndLoginStatus(String emailId, LoginStatusEnum loggedIn,  RolesEnum vendor);

	Optional<Login> findByEmailIdAndLoginStatus(String emailId, LoginStatusEnum loggedIn);

	//Optional<Login> findByEmailIdAndLoginStatusAndRoles(LoginStatusEnum loggedIn, RolesEnum vendor);

	Optional<Login> findByLoginStatus(LoginStatusEnum loggedIn);

	

	
 
	

	
	
	
	
}
