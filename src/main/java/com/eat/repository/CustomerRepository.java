package com.eat.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eat.entity.Customer;
 
@Repository
public interface CustomerRepository extends JpaRepository<Customer,Integer> {

	Optional<Customer> findByemailId(String emailId);
	//List<Customer> findByEmailIdAndContactNoAndCustomerId(String emailId, String contactNo, String customerId);

	
	
	
}