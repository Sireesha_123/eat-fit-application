package com.eat.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eat.entity.FoodItems;

public interface FoodItemRepository extends JpaRepository<FoodItems, Integer>{

}
