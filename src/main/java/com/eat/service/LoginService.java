package com.eat.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.eat.dto.APIresponse;
import com.eat.entity.Customer;
import com.eat.entity.Login;
import com.eat.entity.LoginStatusEnum;
import com.eat.entity.RolesEnum;
import com.eat.entity.Vendor;
import com.eat.repository.CustomerRepository;
import com.eat.repository.LoginRepository;
import com.eat.repository.VenderRepository;

@Service
public class LoginService {
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private VenderRepository vendorRepository;
	@Autowired
	private LoginRepository loginRepository;

	public APIresponse<?> login(String emailId, String password) {
		Optional<Customer> customerRegistrationOptional = customerRepository.findByemailId(emailId);
		Optional<Vendor> vendorOptional = vendorRepository.findByemailId(emailId);
		Optional<Login> loggedInUser = loginRepository.findByEmailIdAndLoginStatus(emailId, LoginStatusEnum.LOGGED_IN);
		if (loggedInUser.isPresent()) {
			if (customerRegistrationOptional.isPresent()) {

				return createErrorResponse(HttpStatus.BAD_REQUEST, "User already logged in");
				
			} else if (vendorOptional.isPresent()) {
				return createErrorResponse(HttpStatus.BAD_REQUEST, "Vendor already logged in");
				
			}
		}

		if (customerRegistrationOptional.isPresent()
				&& customerRegistrationOptional.get().getPassword().equals(password)) {
			loginRepository.save(new Login(emailId, password, RolesEnum.USER, LoginStatusEnum.LOGGED_IN));
			APIresponse<Customer> response = new APIresponse<>();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Customer login successful");
			return response;
		}

		
		else if (vendorOptional.isPresent() && vendorOptional.get().getPassword().equals(password)) {
			loginRepository.save(new Login(emailId, password, RolesEnum.VENDOR, LoginStatusEnum.LOGGED_IN));
			APIresponse<Vendor> response = new APIresponse<>();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Vendor login successful");
			return response;
			
		} else {
			return createErrorResponse(HttpStatus.UNAUTHORIZED, "Invalid Email or password");
			 
		}
	}

	private APIresponse<?> createErrorResponse(HttpStatus UNAUTHORIZED, String messege) {
		return createErrorResponse(UNAUTHORIZED, messege);
		
		
	}

	
}
