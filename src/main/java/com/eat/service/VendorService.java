package com.eat.service;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eat.dto.APIresponse;
import com.eat.dto.Vendordto;
import com.eat.entity.Vendor;
import com.eat.exception.UserAlreadyExists;
import com.eat.repository.VenderRepository;
@Service
public class VendorService {
	@Autowired
	private VenderRepository venderRepository;

	public APIresponse register(Vendordto vendordto) {
		Optional<Vendor> user = venderRepository.findByemailId(vendordto.getEmailId());
		if (user.isPresent()) {
			throw new UserAlreadyExists("Vender already Registered");
		}
		Vendor vender = new Vendor();

		vender.setVenderName(vendordto.getVenderName());
		vender.setPhoneNo(vendordto.getPhoneNo());
		vender.setEmailId(vendordto.getEmailId());
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String password = RandomStringUtils.random(15, characters);
		vender.setPassword(password);
		vender.setRole("Vender");
		//customer.setLoginStatus(LoginStatusEnum.LOGGED_OUT);
		venderRepository.save(vender);
		APIresponse api = new APIresponse();
		api.setStatus(201);
		api.setMessage("Registration successful");
		return api;
}
}