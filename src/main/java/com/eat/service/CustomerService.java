package com.eat.service;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eat.dto.APIresponse;
import com.eat.dto.Customerdto;
import com.eat.entity.Customer;
import com.eat.exception.UserAlreadyExists;
import com.eat.repository.CustomerRepository;


@Service
public class CustomerService {
	@Autowired
	private CustomerRepository customerRepository;

	public APIresponse register(Customerdto customerDto) {
		Optional<Customer> user = customerRepository.findByemailId(customerDto.getEmailId());
		if (user.isPresent()) {
			throw new UserAlreadyExists("Customer already Registered");
		}
		Customer customer = new Customer();

		customer.setCustomerName(customerDto.getCustomerName());
		customer.setContactNo(customerDto.getContactNo());
		customer.setEmailId(customerDto.getEmailId());
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String password = RandomStringUtils.random(15, characters);
		customer.setPassword(password);
		customer.setRole("Customer");
		//customer.setLoginStatus(LoginStatusEnum.LOGGED_OUT);
		customerRepository.save(customer);
		APIresponse api = new APIresponse();
		api.setStatus(201);
		api.setMessage("Registration successful");
		return api;
		}
	}

