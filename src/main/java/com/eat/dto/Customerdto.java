package com.eat.dto;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customerdto {
	@NotBlank(message = "name is requiredfield")
	private String CustomerName;
	@NotBlank(message = "emailid is required field")
	@Email(message = "emailid must be a valid eamil adress")
	private String emailId;
	//@NotBlank(message = "mobileNumber is required")
	//@Size(min = 10, max = 10)
	private long contactNo;

	

}
