package com.eat.dto;

import org.springframework.http.HttpStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "API Response")
public class APIresponse<T> {
	@ApiModelProperty(value = "HTTP status code", example = "200")
	private int status;

	@ApiModelProperty(value = "Message", example = "Success")
	private String message;

	public static Object Status(HttpStatus unauthorized) {
		// TODO Auto-generated method stub
		return null;
	}	
}
