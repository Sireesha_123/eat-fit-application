package com.eat.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodItemdto {
	@NotBlank(message = "Foodname is requiredfield")
	private String FoodName;
	@NotBlank(message = "FoodType is requiredfield")
	private String FoodType;
	@NotBlank(message = "Price is requiredfield")
	 private int Price;
}
