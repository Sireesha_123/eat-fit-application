package com.eat.dto;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Vendordto {
	@NotBlank(message="VenderName cannot be blank")
    @Size(max = 100)
	private String VenderName;
	@NotNull(message="Phone number is required")
	private Long PhoneNo;
	@NotBlank(message="Email id is required")
	@Email(message="Invalid format")
	private String emailId;

}
