package com.eat.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eat.dto.APIresponse;
import com.eat.service.LoginService;

import lombok.AllArgsConstructor;
@AllArgsConstructor
@RestController
@RequestMapping("/")
public class LoginController {
	private LoginService loginService;
	 
	@PostMapping("/Login")
	   public APIresponse<?> login(@RequestParam String emailId, @RequestParam String password) {
	       return loginService.login(emailId, password);
	   }
		//return new ResponseEntity<>(loginService.login(loginDTO), HttpStatus.OK);
}
