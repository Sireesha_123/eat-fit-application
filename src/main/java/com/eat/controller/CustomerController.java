package com.eat.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eat.dto.APIresponse;
import com.eat.dto.Customerdto;
import com.eat.entity.Customer;
import com.eat.service.CustomerService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
@AllArgsConstructor
@RestController
@RequestMapping("/Customer")
public class CustomerController {
	private CustomerService customerrService;
	@PostMapping("/register")
	public ResponseEntity<APIresponse<Customer>> registerUser(@Valid @RequestBody Customerdto register) {
		return new ResponseEntity<>(customerrService.register(register),HttpStatus.CREATED);

	}

}
