package com.eat.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eat.dto.APIresponse;
import com.eat.dto.Vendordto;
import com.eat.entity.Vendor;
import com.eat.service.VendorService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
@AllArgsConstructor
@RestController
@RequestMapping("/")
public class VendorController {
private VendorService vendorService;
	
	@PostMapping("/register")
	public ResponseEntity<APIresponse<Vendor>> registerUser(@Valid @RequestBody Vendordto venderdto) {
		return new ResponseEntity<>(vendorService.register(venderdto),HttpStatus.CREATED);
	}
}
